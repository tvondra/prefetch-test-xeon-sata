#include <stdlib.h>

/* random pattern */

typedef struct random_state_t
{
	unsigned long int	npages;
} random_state_t;

void random_init(random_state_t *state, unsigned long int npages);
unsigned long int random_next(void *data);

/* sequential pattern, with per-page probability */
typedef struct sequential_state_t
{
	unsigned long int	npages;
	unsigned long int	next;
	float				start_probability;
	float				continue_probability;
} sequential_state_t;

void sequential_init(sequential_state_t *state, unsigned long int npages, float start_prob, float cont_prob);
unsigned long int sequential_next(void *data);

typedef unsigned long int (*page_generator_cb) (void *data);
